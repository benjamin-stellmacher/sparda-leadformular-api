<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Lead;

class LeadController extends Controller
{
	public function index()
	{
		return Lead::pluck('json');
	}

	public function show($id)
	{
		return Lead::find($id)->pluck('json');
	}

	public function new()
	{
		$newLeads = Lead::where('retrieved_at', NULL)
			->pluck('json');

		Lead::where('retrieved_at', NULL)
			->update(['retrieved_at' => Carbon::now()]);

		return $newLeads;
	}

	public function store(Request $request)
	{
		return Lead::create($request->all());
	}

	public function update(Request $request, $id)
	{
		$lead = Lead::findOrFail($id);
		$lead->update($lead->all());

		return $lead;
	}

	public function delete(Request $request, $id)
	{
		$lead = Lead::findOrFail($id);
		$lead->delete();

		return 204;
	}
}
