<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['client']], function () {
	Route::get('leads', 'LeadController@index');
	Route::get('leads/new', 'LeadController@new');
	Route::get('lead/{lead}', 'LeadController@show');
	//Route::post('leads', 'LeadController@store');
	//Route::put('leads/{lead}', 'LeadController@update');
	//Route::delete('leads/{lead}', 'LeadController@delete');
});
